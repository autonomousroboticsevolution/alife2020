This repository contain a snapshot of the source of the ARE Framework. This code was used for the experiments of the paper : "Sample and time efficient policy learning with CMA-ES and Bayesian Optimisation"  publish at ALIFE 2020.
The algorithms NBO-ES and NIP-ES are implemented in the following repository : 

- NBO-ES : [experiments/bo_cmaes](https://bitbucket.org/autonomousroboticsevolution/alife2020/src/master/experiments/bo_cmaes/)
- NIP-ES : [experiments/ipop_cmaes](https://bitbucket.org/autonomousroboticsevolution/alife2020/src/master/experiments/ipop_cmaes/)

# Autonomous Robotic Evolution Framework

## Introduction

The ARE Framework is an evolutionary robotic framework made within the ARE project. It is structured around an evolutionary algorithm structure allowing to evolve morphologies and controllers. Currently, it is designed as a plugin to the V-REP simulator (Coppelia). The following instruction is related to the master branch. 

After, having successfully installed the framework, you can refer to the next tutorial: [How to define an experiment within the ARE Framework](https://bitbucket.org/autonomousroboticsevolution/evolutionary_robotics_framework/wiki/Defining%20an%20experiment%20within%20the%20ARE%20Framework).

## Required software

* Preferably use Ubuntu 18.04 as OS
* V-REP Pro Edu 3.6.2 - [link](https://www.coppeliarobotics.com/files/V-REP_PLAYER_V3_6_2_Ubuntu18_04.tar.xz)
* MultiNEAT -  [link](https://github.com/ci-group/MultiNEAT)
* Polyvox - [link](https://github.com/portaloffreedom/polyvox) 
* libdlibxx - [link](https://github.com/m-renaud/libdlibxx)
* Boost
* Eigen 3

## Installation

### Dependencies 

These instructions are specific to Ubuntu (preferably 18.04).

First, install the libraries available via aptitude :
```
sudo apt install libboost-all-dev libeigen3-dev 
```

Install MutliNEAT :
```
git clone https://github.com/ci-group/MultiNEAT.git
cd MultiNEAT
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/install/path  ..
make 
make install # add sudo before the command if the install prefix is /usr/local (default value)
```

Install Polyvox :
```
git clone https://github.com/portaloffreedom/polyvox.git
cd polyvox
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/install/path  ..
make 
make install # add sudo before the command if the install prefix is /usr/local (default value)
```

Install libdlibxx :
```
git clone https://github.com/m-renaud/libdlibxx.git
cd libdlibxx
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/install/path  ..
make 
make install # add sudo before the command if the install prefix is /usr/local (default value)
```

### The framework

In order to install the framework
```
git clone https://legoffl@bitbucket.org/autonomousroboticsevolution/evolutionary_robotics_framework.git
cd evolutionary_robotics_framework
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/install/path -DVREP_FOLDER=/vrep/folder/path ..
make
make install # add sudo if the install prefix is /usr/local/ (default value)
```
If you would like to use CLion IDE with this project please follow the instructions in the [report](https://www.overleaf.com/8988212588bdkjhpfdtckz).

## How to launch an experiment

You can launch experiments in two different ways : in local mode or in remote mode. The local mode is for launching sequential experiments only, it is meant to be a debug mode. The remote mode is for launching several simulator instances and thus launch an experiment in parallel.

### Local Mode

```
cd v_rep_folder
./vrep.sh -h -g/path/to/exp/parameters/file/parameters.csv
```
-h option is for headless, which means without a GUI. The parameters file is a csv file in which all the parameters of your experiment are written. You can find an example [here](https://bitbucket.org/autonomousroboticsevolution/evolutionary_robotics_framework/src/restruct_refact/EvolutionVREP/experiments/hyperneat/parameters.csv).
All the logs of your experiment are stored in a folder named after the name of your experiment (specified in the parameters file) and the date of its launch. This folder is itself in the repository folder you specified in the parameters file.

### Remote Mode

```
cd are_framework_folder/EvolutionVREP/Cluster
python3 run_cluster.py N --vrep vrep_folder/vrep.sh --client are_framework_folder/EvolutionVREP/ERClient/ERClient --params /path/to/exp/parameters/file/parameters.csv --port_start PORT_START --xvbf (0|1)
```
N corresponds to the number of simulator instances you want to launch which should not be greater than the number of core. PORT_START will be the communication port of the first simulator instance, then PORT_START + 1 will the port for the second instance and so on.
the --xvbf option is to be set at 1 if you launch your experiment through ssh. This script will launch N simulator instance and a client process which handles the experiment.

Like in local mode, all the ogs of your experiment are stored in a folder named after the name of your experiment (specified in the parameters file) and the date of its launch. This folder is itself in the repository folder you specified in the parameters file.
As well, you have log files corresponding to the outputs of the client and all the simulator instances stored where you launched your experiment. This files are named client_<date> for the client output and sim_<nb-instance>_<date>.lator
* *RobotController* contains all the files necessary to compile the controllers to physical robots.

## Authors

* **Matt Hale**
* **Edgar Buchanan**
* **Wei Li**
* **Matteo de Carlo**
* **Robert Woolley**
* **Léni K. Le Goff**

See also the list of [contributors](https://www.york.ac.uk/robot-lab/are/) who participated in this project.
