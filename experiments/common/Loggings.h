#ifndef LOGGINGS_H
#define LOGGINGS_H

#include "ARE/Logging.h"
#include "ARE/Individual.h"
#include "NNGenome.hpp"

#include "ARE/EA.h"

namespace are{

class FitnessLog : public Logging
{
public:
    FitnessLog(const std::string &file) : Logging(file,true){} //Logging at the end of the generation
    void saveLog(EA::Ptr & ea);
    void loadLog(const std::string& logFile){}
};

class EvalTimeLog : public Logging
{
public:
    EvalTimeLog(const std::string &file) : Logging(file,false){}
    void set_end_of_gen(bool b){endOfGen = b;}
    void saveLog(EA::Ptr & ea);
    void loadLog(const std::string& logFile){}
};

class CtrlNEATGenomeLog : public Logging
{
public:
    CtrlNEATGenomeLog(const std::string &file) : Logging(file,true){} //Logging at the end of the generation
    void saveLog(EA::Ptr & ea);
    void loadLog(const std::string& logFile,EA::Ptr & ea);
};

}//are
#endif //LOGGINGS_H
