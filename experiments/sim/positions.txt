all in metre
ARE_arena :
    start & target:
    	- 0;0;0.05 #centre
		- 0.75;-0.75;0.05 #bottom right corner 
		- -0.75;-0.75;0.05 #bottom left corner 
		- -0.75;0.75;0.05 #upper left corner
		- 0.75;0.75;0.05 #upper right corner

middle_wall :
    start & target:
    	- 0;-0.5;0.05 #bottom part
    	- 0;0.5;0.05 #upper part
		- 0.75;-0.75;0.05 #bottom right corner 
		- -0.75;-0.75;0.05 #bottom left corner 
		- -0.75;0.75;0.05 #upper left corner
		- 0.75;0.75;0.05 #upper right corner

escape_room :
	start:
	    - 0;0;0.05 #centre
	target:
		- 0.75;-0.75;0.05 #bottom right corner 
		- -0.75;-0.75;0.05 #bottom left corner 
		- -0.75;0.75;0.05 #upper left corner
		- 0.75;0.75;0.05 #upper right corner


easy_race :
	start -> target
		- 0;0;0.05 -> #bottom left corner or #upper right corner
		- 0.75;0.75;0.05 -> -0.75;-0.75;0.05
		- -0.75;-0.75;0.05 -> 0.75;0.75;0.05

race : 
	start -> target :
		- -0.75;0.75;0.05 -> 0.75;0.75;0.05 #left to right
		- 0.75;0.75;0.05 -> -0.75;0.75;0.05 #right to left
		- 0;0.725;0.05 -> #left or #right

hard_race :
	start -> target :
		- -0.75;-0.75;0.05 -> 7.5;7.5;0.05 #bottom left to upper right
		- 0.75;0.75;0.05 -> -7.5;-7.5;0.05 #upper right to bottom left
		- 0.25;-0.75;0.25 -> #upper right or bottm left


ARE_hard_maze :
    start :
    	- 0.625;0.625;0.05 #upper right corner
    	- 0.625;-0.25;0.05 #upper dead end
    	- 0.75;-0.75;0.05 #bottom dead end
    	- -1.25;-1.25;0.05 #centre
    target :
    	- 0.625;-0.25;0.05 #upper dead end
    	- 0.75;-0.75;0.05 #bottom dead end

multi_maze :
	start : 1.25;1.25;0.05 #centre
	target (& start) :
		- -0.75;0.75;0.05 #upper left part
		- -0.15;-0.875;0.05 #bottom left part
		- 0.8;-0.55;0.05 #bottom right part
		- 0.5;0.75;0.05  #upper right upper part
		- 0.75;0.45;0.05 #upper right bottom part 



		
